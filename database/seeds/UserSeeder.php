<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Super Admin',
            'email' => 'admin@ziqra.com',
            'email_verified_at' => Carbon::now(),
            'user_level' => 'admin',
            'password' => Hash::make('testing'),
            'created_at' => Carbon::now(), 
            'updated_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
            'name' => 'Baiq Yeni',
            'email' => 'perawat@ziqra.com',
            'email_verified_at' => Carbon::now(),
            'user_level' => 'perawat',
            'password' => Hash::make('testing'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
  
    }
}
