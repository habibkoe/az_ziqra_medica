<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemeriksaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemeriksaans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pasien')->unsigned();
            $table->foreign('id_pasien')
            ->references('id')->on('pasiens')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('jenis_pemeriksaan');
            $table->string('diagnosa');
            $table->string('tindakan_medis');
            $table->string('resep_obat');
            $table->boolean('isDirujuk');
            $table->string('lokasi_rujukan');
            $table->text('alasan_dirujuk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemeriksaans');
    }
}
