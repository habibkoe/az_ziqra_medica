<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasiensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasiens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kode_pasien');
            $table->string('nama_awal');
            $table->string('nama_ahir');
            $table->string('jenis_kelamin');
            $table->date('tanggal_lahir');
            $table->string('alamat');
            $table->string('nomor_telepon');
            $table->string('nama_wali');
            $table->string('telpon_wali');
            $table->string('hubungan_dengan_wali');
            $table->string('jenis_pasien');//umum, bpjs,asuransi
            $table->string('nomor_jaminan'); //jika pasien bpjs, atau yang lain
            $table->string('nama_asuransi'); //jika pasien asuransi
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasiens');
    }
}
