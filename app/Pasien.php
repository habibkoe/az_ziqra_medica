<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{

    protected $guarded = [];

    public function kunjungan()
    {
        return $this->hasMany('App\Kunjungan', 'id_pasien', 'id');
    }

    public function pemeriksaan()
    {
        return $this->hasMany('App\Pemeriksaan', 'id_pasien', 'id');
    }
    //
}
