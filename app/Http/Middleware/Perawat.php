<?php

namespace App\Http\Middleware;

use Closure;

class Perawat
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->user_level == 'perawat'){
            return $next($request);
        }
        return redirect('/')->with('error','ini hanya untuk perawat');
    }
}
