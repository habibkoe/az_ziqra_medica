<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserController extends Controller
{
    private $sidebar = "users";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sidebar = $this->sidebar;
        $title = "Users - Az Ziqra Medica";
        $users = User::all();
        return view('auth.index', compact('users', 'title','sidebar'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterRequest $request)
    {
        $password = $request->get('password');
        $password_confirm = $request->get('password_confirmation');

        if($password==$password_confirm){

            $user = User::create([
                'name' => request('name'),
                'email' => request('email'),
                'user_level' => request('user_level'),
                'password' => Hash::make(request('password'))
            ]);

            if($user){
                return redirect('/users');
            }
        }
        // $user = User::create($request->all());
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $password_cek = request('password');
        $password_confirm_cek = request('password_confirmation');
        $password_old = request('password_old');

        if($password_cek != null){
            $password = Hash::make($password_cek);
        }else{
            $password = $password_old;
        }
        
        $user= User::find($id);
        $user->name=$request->get('name');
        $user->email=$request->get('email');
        $user->user_level=$request->get('user_level');
        $user->password=$password;
        $user->updated_at = Carbon::now();
        $user->save();
        return redirect('/users');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id); 
        return redirect('users')->with('success', 'Item Has Been Delete');
        //
    }
}
