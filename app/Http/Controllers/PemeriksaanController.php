<?php

namespace App\Http\Controllers;

use App\Pasien;
use App\Pemeriksaan;
use Illuminate\Http\Request;
use App\Http\Requests\EntryCheckUpRequest;
use App\Exports\PemeriksaanExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class PemeriksaanController extends Controller
{
    private $sidebar = "check_up";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sidebar = $this->sidebar;
        $title = "Pemeriksaan - Az Ziqra Medica";
        $history_checkup = Pemeriksaan::all();
        return view('pemeriksaan.index', compact('history_checkup', 'title','sidebar'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function chartKunjungan()
    {

        $pemeriksaan = Pemeriksaan::select(DB::raw('count(id_pasien) as jml_pasien, month(created_at) as bulan'))->groupBy('bulan')->get();

        // $pemeriksaan = Pemeriksaan::all();            
        return response()->json($pemeriksaan);
    }

    public function create()
    {
        $pasiens = Pasien::all();
        $sidebar = $this->sidebar;
        $title = "Check Up - Az Ziqra Medica";
        return view('pemeriksaan.check_up_new', compact('pasiens', 'title','sidebar'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntryCheckUpRequest $request)
    {
        $is_dirujuk = $request->get('isDirujuk');
        $id_pasien = $request->get('id_pasien');
        
        if($is_dirujuk==1){
            $lokasi_rujukan = request('lokasi_rujukan');
            $alasan_dirujuk = request('alasan_dirujuk');
        }else{
            $lokasi_rujukan = "-";
            $alasan_dirujuk = "-";
        }
        $pasien = Pemeriksaan::create([
            'id_pasien' => $id_pasien,
            'jenis_pemeriksaan' => request('jenis_pemeriksaan'),
            'diagnosa' => request('diagnosa'),
            'tindakan_medis' => request('tindakan_medis'),
            'resep_obat' => request('resep_obat'),
            'isDirujuk' => $is_dirujuk,
            'lokasi_rujukan' => $lokasi_rujukan,
            'alasan_dirujuk' => $alasan_dirujuk
        ]);

        if($pasien){
            return redirect("/pasiens/$id_pasien");
        }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pemeriksaan  $pemeriksaan
     * @return \Illuminate\Http\Response
     */
    public function show(Pemeriksaan $pemeriksaan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pemeriksaan  $pemeriksaan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sidebar = $this->sidebar;
        $pasien = Pasien::find($id);
        $nama_pasien = "$pasien->nama_awal $pasien->nama_ahir";
        $title = "Check Up Pasien $nama_pasien - Az Ziqra Medica";
        return view('pemeriksaan.check_up', compact('pasien', 'title','sidebar'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pemeriksaan  $pemeriksaan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pemeriksaan $pemeriksaan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pemeriksaan  $pemeriksaan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pemeriksaan $pemeriksaan)
    {
        //
    }

    public function export() 
    {

        return Excel::download(new PemeriksaanExport, 'pemeriksaan.xlsx');
    }
}
