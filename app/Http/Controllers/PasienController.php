<?php

namespace App\Http\Controllers;

use App\Pasien;
use App\Pemeriksaan;
use Illuminate\Http\Request;
use App\Http\Requests\TambahPasienRequest;
use Carbon\Carbon;

class PasienController extends Controller
{
    private $sidebar = "pasiens";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sidebar = $this->sidebar;
        $title = "Data Pasien - Az Ziqra Medica";
        $data = Pasien::orderBy('id', 'desc')->get();
        // $data = Pasien::all();
        // $data = Pasien::orderBy('id', 'desc')->paginate(15);

        return view('pasien.index', compact('data', 'title','sidebar'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TambahPasienRequest $request)
    {
        $jenis_pasien = $request->get('jenis_pasien');
        
        if($jenis_pasien=='BPJS'){
            $nomor_jaminan = request('nomor_jaminan');
            $nama_asuransi = "-";
        }else if($jenis_pasien=='asuransi'){
            $nomor_jaminan = "-";
            $nama_asuransi = request('nama_asuransi');
        }else {
            $nomor_jaminan = "-";
            $nama_asuransi = "-";
        }

        $user = Pasien::create([
            'kode_pasien' => time(),
            'nama_awal' => request('nama_awal'),
            'nama_ahir' => request('nama_ahir'),
            'jenis_kelamin' => request('jenis_kelamin'),
            'tanggal_lahir' => request('tanggal_lahir'),
            'jenis_pasien' => request('jenis_pasien'),
            'nomor_jaminan' => $nomor_jaminan,
            'nama_asuransi' => $nama_asuransi,
            'alamat' => request('alamat'),
            'nomor_telepon' => request('nomor_telepon'),
            'nama_wali' => request('nama_wali'),
            'telpon_wali' => request('telpon_wali'),
            'hubungan_dengan_wali' => request('hubungan_dengan_wali')
        ]);

        if($user){
            return redirect('/pasiens');
        }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pasien  $pasien
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sidebar = $this->sidebar;
        $data = Pasien::find($id);
        $history_checkup = Pemeriksaan::whereIdPasien($id)->get();
        $nama_pasien = "$data->nama_awal $data->nama_ahir";
        $title = "Detail Data Pasien $nama_pasien - Az Ziqra Medica";
        return view('pasien.view', compact('data','history_checkup','title','sidebar'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pasien  $pasien
     * @return \Illuminate\Http\Response
     */
    public function edit(Pasien $pasien)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pasien  $pasien
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $jenis_pasien = $request->get('jenis_pasien');
        
        if($jenis_pasien=='BPJS'){
            $nomor_jaminan = request('nomor_jaminan');
            $nama_asuransi = "-";
        }else if($jenis_pasien=='asuransi'){
            $nomor_jaminan = "-";
            $nama_asuransi = request('nama_asuransi');
        }else {
            $nomor_jaminan = "-";
            $nama_asuransi = "-";
        }

        $pasien= Pasien::find($id);
        $pasien->nama_awal=$request->get('nama_awal');
        $pasien->nama_ahir=$request->get('nama_ahir');
        $pasien->jenis_kelamin=$request->get('jenis_kelamin');
        $pasien->tanggal_lahir=$request->get('tanggal_lahir');
        $pasien->jenis_pasien=$request->get('jenis_pasien');
        $pasien->nomor_jaminan= $nomor_jaminan;
        $pasien->nama_asuransi= $nama_asuransi;
        $pasien->alamat=$request->get('alamat');
        $pasien->nomor_telepon=$request->get('nomor_telepon');
        $pasien->nama_wali=$request->get('nama_wali');
        $pasien->telpon_wali=$request->get('telpon_wali');
        $pasien->hubungan_dengan_wali=$request->get('hubungan_dengan_wali');
        $pasien->updated_at = Carbon::now();
        $pasien->save();
        return redirect('/pasiens');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pasien  $pasien
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pasien::destroy($id);
        return redirect('pasiens')->with('success', 'Item Has Been Delete');

        //
    }
}
