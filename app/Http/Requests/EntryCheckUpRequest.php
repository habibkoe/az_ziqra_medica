<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EntryCheckUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_pasien' => 'required',
            'jenis_pemeriksaan' => 'required|string|max:255',
            'diagnosa' => 'required|string',
            'tindakan_medis' => 'required|string',
            'resep_obat' => 'required|string',
            'isDirujuk' => 'required'
            //
        ];
    }
}
