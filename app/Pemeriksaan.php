<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemeriksaan extends Model
{

    protected $guarded = [];

    public function pasien()
    {
        return $this->belongsTo('App\Pasien', 'id_pasien', 'id');
    }
    //
}
