@extends('layouts.app')

@section('css')
{{--
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"> --}}
<style>
    .telang {
        display: none;
    }
</style>
@endsection

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 my-4">
    <a href="{{ route('pasien_checkup', $data->id) }}" class="btn btn-success btn-sm"><i class="fas fa-user-md"></i>
        Check Up</a>
    <button style="float: right;" type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#edit_pasien_{{$data->id}}"
        title="Edit"><i class="far fa-edit" data-toggle="tooltip" data-placement="left" title="Edit Data Pasien"></i></button>
    <br>
    <br>
    <div class="table-responsive">
        <table class="table table-bordered table-sm">
            <tr>
                <td width="15%">RM</td>
                <td>{{$data->kode_pasien}}</td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>{{$data->nama_awal}} {{$data->nama_ahir}}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>{{$data->jenis_kelamin}}</td>
            </tr>
            <tr>
                <td>Tanggal Lahir</td>
                <td>{{$data->tanggal_lahir}}</td>
            </tr>
            <tr>
                <td>Jenis Pasien</td>
                <td>{{$data->jenis_pasien}} </td>
            </tr>
            @if($data->jenis_pasien=='BPJS')
            <tr>
                <td>Nomor BPJS</td>
                <td>{{$data->nomor_jaminan}} </td>
            </tr>
            @endif
            @if($data->jenis_pasien=='asuransi')
            <tr>
                <td>Nama Asuransi</td>
                <td>{{$data->nama_asuransi}} </td>
            </tr>
            @endif
            <tr>
                <td>Alamat</td>
                <td>{{$data->alamat}}</td>
            </tr>
            <tr>
                <td>Nomor Telepon</td>
                <td>{{$data->nomor_telepon}}</td>
            </tr>
            <tr>
                <td>Hubungan Dengan Wali</td>
                <td>{{$data->hubungan_dengan_wali}}</td>
            </tr>
            <tr>
                <td>Nama Wali</td>
                <td>{{$data->nama_wali}}</td>
            </tr>
            <tr>
                <td>Nomor Telpon Wali</td>
                <td>{{$data->telpon_wali}}</td>
            </tr>
        </table>
    </div>
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Jenis Pemeriksaan</th>
                    <th>Diagnosa</th>
                    <th>Tindakan Medis</th>
                    <th>Resep Obat</th>
                    <th>Tanggal Periksa</th>
                    <th width="13%">Setting</th>
                </tr>
            </thead>
            <tbody>
                @if(!$history_checkup) 
                    <tr>
                        <td colspan="6">Belum ada data</td>
                    </tr>
                @else
                @foreach ($history_checkup as $index => $value)
                    <tr>
                        <td>{{$index+1}}</td>
                        <td>{{$value->jenis_pemeriksaan}}</td>
                        <td>{{$value->diagnosa}}</td>
                        <td>{{$value->tindakan_medis}}</td>
                        <td>{{$value->resep_obat}}</td>
                        <td>{{ date("d-m-Y", strtotime($value->created_at)) }}</td>
                        <td>
                            <button style="float: left;" type="button" class="btn btn-warning btn-sm mr-2" data-toggle="modal"
                                data-target="#edit_pemeriksaan_{{$value->id}}" title="Edit"><i class="far fa-edit" data-toggle="tooltip" data-placement="left" title="Edit Pemeriksaan Pasien"></i></button>
                            {{-- modaal edit pemeriksaan pasien --}}
                        <div class="modal fade" id="edit_pemeriksaan_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="edit_pemeriksaan_{{$value->id}}" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Pemeriksaan {{$value->pasien->nama_awal}} {{$value->pasien->nama_ahir}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                                <form method="POST" action="{{ route('pasien_checkup_store') }}">
                                                        @csrf
                                                        <div class="form-group row">
                                                            <label for="id_pasien" class="col-md-4 col-form-label text-md-right">Nama Pasien</label>
                                                
                                                            <div class="col-md-6">
                                                                <input type="hidden" name="id_pasien" value="{{$value->pasien->id}}">
                                                                <input id="nama_pasien" type="text" class="form-control{{ $errors->has('nama_pasien') ? ' is-invalid' : '' }}"
                                                                name="nama_pasien" value="{{$value->pasien->nama_awal}} {{$value->pasien->nama_ahir}}" disabled>
                                                            </div>
                                                        </div>
                                                
                                                        <div class="form-group row">
                                                                <label for="jenis_pemeriksaan" class="col-md-4 col-form-label text-md-right">Jenis Pemeriksaan</label>
                                                    
                                                                <div class="col-md-6">
                                                                    <select name="jenis_pemeriksaan" id="jenis_pemeriksaan" class="form-control" required autofocus>
                                                                        <option value="Fisik">Fisik</option>
                                                                        <option value="Terapi">Terapi</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                
                                                        <div class="form-group row">
                                                            <label for="diagnosa" class="col-md-4 col-form-label text-md-right">Diagnosa</label>
                                                
                                                            <div class="col-md-6">
                                                            <textarea name="diagnosa" id="diagnosa" cols="30" rows="3" class="form-control" required>{{$value->diagnosa}}</textarea>
                                                
                                                                @if ($errors->has('diagnosa'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('diagnosa') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                
                                                        <div class="form-group row">
                                                            <label for="tindakan_medis" class="col-md-4 col-form-label text-md-right">Tindakan Medis</label>
                                                
                                                            <div class="col-md-6">
                                                                <input id="tindakan_medis" type="text" class="form-control{{ $errors->has('tindakan_medis') ? ' is-invalid' : '' }}"
                                                                    name="tindakan_medis" required value="{{$value->tindakan_medis}}">
                                                
                                                                @if ($errors->has('tindakan_medis'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('tindakan_medis') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                
                                                        <div class="form-group row">
                                                            <label for="resep_obat" class="col-md-4 col-form-label text-md-right">Resep Obat</label>
                                                
                                                            <div class="col-md-6">
                                                                <input id="resep_obat" type="text" class="form-control{{ $errors->has('resep_obat') ? ' is-invalid' : '' }}"
                                                                    name="resep_obat" required value="{{$value->resep_obat}}">
                                                
                                                                @if ($errors->has('resep_obat'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('resep_obat') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                
                                                        <div class="form-group row">
                                                            <label for="alamat" class="col-md-4 col-form-label text-md-right">Apakah Pasien Dirujuk.?</label>
                                                
                                                            <div class="col-md-6">
                                                                <input type="radio" name="isDirujuk" id="isDirujuk" value="1" onclick="show2({{$value->id}});"> Ya
                                                                <br>
                                                                <input type="radio" name="isDirujuk" id="isDirujuk" value="0" onclick="show1({{$value->id}});"> Tidak
                                                            </div>
                                                        </div>
                                                        <div id="div1{{$value->id}}" class="telang">
                                                            <div class="form-group row">
                                                                <label for="lokasi_rujukan" class="col-md-4 col-form-label text-md-right">Lokasi Rujukan</label>
                                                
                                                                <div class="col-md-6">
                                                                    <input id="lokasi_rujukan" type="text" class="form-control{{ $errors->has('lokasi_rujukan') ? ' is-invalid' : '' }}"
                                                                        name="lokasi_rujukan" value="{{$value->lokasi_rujukan}}">
                                                
                                                                    @if ($errors->has('lokasi_rujukan'))
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('lokasi_rujukan') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                
                                                            <div class="form-group row">
                                                                <label for="alasan_dirujuk" class="col-md-4 col-form-label text-md-right">Alasan Dirujuk</label>
                                                
                                                                <div class="col-md-6">
                                                                <textarea name="alasan_dirujuk" id="alasan_dirujuk" cols="30" rows="5" class="form-control">{{$value->alasan_dirujuk}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                        
                                                <div class="form-group row mb-0">
                                                    <div class="col-md-6 offset-md-4">
                                                        <button type="submit" class="btn btn-primary">
                                                            {{ __('Simpan') }}
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {{-- end modal edit pemeriksaan pasien --}}
                            
                            <form action="{{ route('pasien_delete', $value->id) }}" method="post">
                                @csrf
                                @method("DELETE")
                                <button style="float: left;" type="submit" class="btn btn-danger btn-sm mr-2"
                                    data-toggle="tooltip" data-placement="left" title="Hapus Data Pasien" onclick="return confirm('Apakah anda yakin menghapus data ini.?')"><i
                                        class="fas fa-trash"></i></button>
                            </form>
                            <button style="float: left;" type="button" class="btn btn-primary btn-sm mr-2" data-toggle="modal"
                                data-target="#detail_pemeriksaan_{{$value->id}}" title="Detail"><i class="far fa-eye"
                                    data-toggle="tooltip" data-placement="left" title="Detail Check Up"></i></button>

                            <div class="modal fade" id="detail_pemeriksaan_{{$value->id}}" tabindex="-1" role="dialog"
                                aria-labelledby="detail_pemeriksaan_{{$value->id}}" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Detail Pemeriksaan
                                                {{$value->pasien->nama_awal}} {{$value->pasien->nama_ahir}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-sm">
                                                    <tr>
                                                        <td width="15%">Nama</td>
                                                        <td>{{$data->nama_awal}} {{$data->nama_ahir}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Pemeriksaan</td>
                                                        <td>{{ date("d-m-Y", strtotime($value->created_at)) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jenis Pemeriksaan</td>
                                                        <td>{{$value->jenis_pemeriksaan}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Diagnosa</td>
                                                        <td>{{$value->diagnosa}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tindakan Medis</td>
                                                        <td>{{$value->tindakan_medis}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Resep Obat</td>
                                                        <td>{{$value->resep_obat}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Dirujuk</td>
                                                        <td>@if($value->isDirujuk==0)Tidak @else Ya @endif</td>
                                                    </tr>
                                                    @if($value->isDirujuk==1)
                                                    <tr>
                                                        <td>Lokasi Rujukan</td>
                                                        <td>{{$value->lokasi_rujukan}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Alasan Dirujuk</td>
                                                        <td>{{$value->alasan_dirujuk}}</td>
                                                    </tr>
                                                    @endif
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- end modal detail check up --}}
                        </td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        {{-- {{ $data->links() }} --}}
    </div>
</main>

{{-- modaal edit pasien --}}
<div class="modal fade" id="edit_pasien_{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="edit_pasien_{{$data->id}}"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data {{$data->nama_awal}} {{$data->nama_ahir}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('pasien_edit', $data->id) }}">
                    @csrf
                    @method("PUT")
                    <div class="form-group row">
                        <label for="kode_pasien" class="col-md-4 col-form-label text-md-right">RM</label>

                        <div class="col-md-6">
                            <input id="kode_pasien" type="text" class="form-control" name="kode_pasien" required value="{{ $data->kode_pasien }}"
                                disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Nama</label>

                        <div class="col-md-3">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                name="nama_awal" value="{{ $data->nama_awal }}" placeholder="Nama Awal" required
                                autofocus>

                            @if ($errors->has('nama_awal'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nama_awal') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-3">
                            <input id="nama_ahir" type="text" class="form-control{{ $errors->has('nama_ahir') ? ' is-invalid' : '' }}"
                                name="nama_ahir" value="{{ $data->nama_ahir }}" placeholder="Nama Ahir">

                            @if ($errors->has('nama_ahir'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nama_ahir') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="tanggal_lahir" class="col-md-4 col-form-label text-md-right">Tanggal Lahir</label>

                        <div class="col-md-6">
                            <input id="tanggal_lahir" type="date" class="form-control{{ $errors->has('tanggal_lahir') ? ' is-invalid' : '' }}"
                                name="tanggal_lahir" required value="{{ $data->tanggal_lahir }}">

                            @if ($errors->has('tanggal_lahir'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('tanggal_lahir') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="nomor_bpjs" class="col-md-4 col-form-label text-md-right">Jenis Pasien</label>

                        <div class="col-md-6">
                            <input type="radio" name="jenis_pasien" id="jenis_pasien" value="Umum" onclick="umum();">
                            Pasien Umum
                            <input type="radio" name="jenis_pasien" id="jenis_pasien" value="BPJS" onclick="bpjs();">
                            BPJS
                            <input type="radio" name="jenis_pasien" id="jenis_pasien" value="asuransi" onclick="asuransi();">
                            Asuransi
                            <input id="nomor_jaminan" type="text" class="telang form-control{{ $errors->has('nomor_jaminan') ? ' is-invalid' : '' }}"
                                name="nomor_jaminan" placeholder="Nomor BPJS">
                            @if ($errors->has('nomor_jaminan'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nomor_jaminan') }}</strong>
                            </span>
                            @endif

                            <input id="nama_asuransi" type="text" class="telang form-control{{ $errors->has('nama_asuransi') ? ' is-invalid' : '' }}"
                                name="nama_asuransi" placeholder="Nama Asuransi">
                            @if ($errors->has('nama_asuransi'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nama_asuransi') }}</strong>
                            </span>
                            @endif

                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="alamat" class="col-md-4 col-form-label text-md-right">Alamat</label>

                        <div class="col-md-6">
                            <textarea name="alamat" id="alamat" cols="30" rows="4" class="form-control">{{$data->alamat}}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="jenis_kelamin" class="col-md-4 col-form-label text-md-right">Jenis Kelamin</label>

                        <div class="col-md-6">
                            <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                                <option value="L">Laki - Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="nomor_telepon" class="col-md-4 col-form-label text-md-right">Nomor Telepon</label>

                        <div class="col-md-6">
                            <input id="nomor_telepon" type="text" class="form-control{{ $errors->has('nomor_telepon') ? ' is-invalid' : '' }}"
                                name="nomor_telepon" required value="{{ $data->nomor_telepon }}">

                            @if ($errors->has('nomor_telepon'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nomor_telepon') }}</strong>
                            </span>
                            @endif
                            <small class="text-danger">Password minimal 6 karakter</small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="hubungan_dengan_wali" class="col-md-4 col-form-label text-md-right">Hubungan Dengan
                            Wali</label>

                        <div class="col-md-6">
                            <select name="hubungan_dengan_wali" id="hubungan_dengan_wali" class="form-control">
                                <option value="ibu">Ibu</option>
                                <option value="ayah">Ayah</option>
                                <option value="adik">Adik</option>
                                <option value="kakak">Kakak</option>
                                <option value="bibik">Bibik</option>
                                <option value="paman">Paman</option>
                                <option value="kakek">Kakek</option>
                                <option value="nenek">Nenek</option>
                                <option value="istri">Istri</option>
                                <option value="suami">Suami</option>
                                <option value="tetangga">Tetangga</option>
                                <option value="sahabat">Sahabat</option>
                            </select>

                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama_wali" class="col-md-4 col-form-label text-md-right">Nama Wali</label>

                        <div class="col-md-6">
                            <input id="nama_wali" type="text" class="form-control" name="nama_wali" value="{{$data->nama_wali}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="telpon_wali" class="col-md-4 col-form-label text-md-right">Telpon Wali</label>

                        <div class="col-md-6">
                            <input id="telpon_wali" type="text" class="form-control" name="telpon_wali" value="{{$data->telpon_wali}}">
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Simpan') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{{-- end modal edit pasien --}}
@endsection

@section('js')
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    function asuransi(){
        document.getElementById('nama_asuransi').style.display ='block';
        document.getElementById('nomor_jaminan').style.display ='none';
    }
    function umum(){
        document.getElementById('nomor_jaminan').style.display ='none';
        document.getElementById('nama_asuransi').style.display ='none';
    }
    function bpjs(){
        document.getElementById('nomor_jaminan').style.display = 'block';
        document.getElementById('nama_asuransi').style.display ='none';
    }
    function show1(id){
        document.getElementById('div1'+ id).style.display ='none';
    }
    function show2(id){
        document.getElementById('div1' + id).style.display = 'block';
    }
</script>
@endsection