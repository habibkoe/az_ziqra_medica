@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <style>
            .telang {
                display: none;
            }
        </style>
@endsection

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 my-4">
    <h2>Data Pasien <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#add_pasien"><i
                class="fas fa-user-plus"></i></button></h2>

    {{-- Modal tambah data --}}
    <div class="modal fade" id="add_pasien" tabindex="-1" role="dialog" aria-labelledby="add_pasien" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('pasien_new') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Nama</label>

                            <div class="col-md-3">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                    name="nama_awal" placeholder="Nama Awal" required autofocus>

                                @if ($errors->has('nama_awal'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nama_awal') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-3">
                                <input id="nama_ahir" type="text" class="form-control{{ $errors->has('nama_ahir') ? ' is-invalid' : '' }}"
                                    name="nama_ahir" placeholder="Nama Ahir">

                                @if ($errors->has('nama_ahir'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nama_ahir') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tanggal_lahir" class="col-md-4 col-form-label text-md-right">Tanggal Lahir</label>

                            <div class="col-md-6">
                                <input id="tanggal_lahir" type="date" class="form-control{{ $errors->has('tanggal_lahir') ? ' is-invalid' : '' }}"
                                    name="tanggal_lahir" required>

                                @if ($errors->has('tanggal_lahir'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('tanggal_lahir') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nomor_bpjs" class="col-md-4 col-form-label text-md-right">Jenis Pasien</label>

                            <div class="col-md-6">
                                <input type="radio" name="jenis_pasien" id="jenis_pasien" value="Umum" onclick="umum();"> Pasien Umum 
                                <input type="radio" name="jenis_pasien" id="jenis_pasien" value="BPJS" onclick="bpjs();"> BPJS
                                <input type="radio" name="jenis_pasien" id="jenis_pasien" value="asuransi" onclick="asuransi();"> Asuransi
                                <input id="nomor_jaminan" type="text" class="telang form-control{{ $errors->has('nomor_jaminan') ? ' is-invalid' : '' }}"
                                    name="nomor_jaminan" placeholder="Nomor BPJS">
                                @if ($errors->has('nomor_jaminan'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nomor_jaminan') }}</strong>
                                </span>
                                @endif

                                <input id="nama_asuransi" type="text" class="telang form-control{{ $errors->has('nama_asuransi') ? ' is-invalid' : '' }}"
                                    name="nama_asuransi" placeholder="Nama Asuransi">
                                @if ($errors->has('nama_asuransi'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nama_asuransi') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-right">Alamat</label>

                            <div class="col-md-6">
                                <textarea name="alamat" id="alamat" cols="30" rows="4" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jenis_kelamin" class="col-md-4 col-form-label text-md-right">Jenis Kelamin</label>

                            <div class="col-md-6">
                                <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                                    <option value="L">Laki - Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nomor_telepon" class="col-md-4 col-form-label text-md-right">Nomor Telepon</label>

                            <div class="col-md-6">
                                <input id="nomor_telepon" type="text" class="form-control{{ $errors->has('nomor_telepon') ? ' is-invalid' : '' }}"
                                    name="nomor_telepon" required>

                                @if ($errors->has('nomor_telepon'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nomor_telepon') }}</strong>
                                </span>
                                @endif
                                <small class="text-danger">Password minimal 6 karakter</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hubungan_dengan_wali" class="col-md-4 col-form-label text-md-right">Hubungan Dengan Wali</label>

                            <div class="col-md-6">
                                <select name="hubungan_dengan_wali" id="hubungan_dengan_wali" class="form-control">
                                    <option value="ibu">Ibu</option>
                                    <option value="ayah">Ayah</option>
                                    <option value="adik">Adik</option>
                                    <option value="kakak">Kakak</option>
                                    <option value="bibik">Bibik</option>
                                    <option value="paman">Paman</option>
                                    <option value="kakek">Kakek</option>
                                    <option value="nenek">Nenek</option>
                                    <option value="istri">Istri</option>
                                    <option value="suami">Suami</option>
                                    <option value="tetangga">Tetangga</option>
                                    <option value="sahabat">Sahabat</option>
                                </select>
                                
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="nama_wali" class="col-md-4 col-form-label text-md-right">Nama Wali</label>
    
                                <div class="col-md-6">
                                    <input id="nama_wali" type="text" class="form-control" name="nama_wali">
                                </div>
                            </div>
                        <div class="form-group row">
                            <label for="telpon_wali" class="col-md-4 col-form-label text-md-right">Telpon Wali</label>

                            <div class="col-md-6">
                                <input id="telpon_wali" type="text" class="form-control" name="telpon_wali">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Simpan') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- end modal tambah data --}}
    <div class="table-responsive">
        <table id="pasien" class="table table-bordered table-hover table-sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Nomor Telepon</th>
                    <th>Jenis Kelamin</th>
                    <th>Setting</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $index => $value)
                <tr>
                    <td>{{$index+1}}</td>
                    <td>{{$value->nama_awal}} {{$value->nama_ahir}} <br> RM: {{$value->kode_pasien}}</td>
                    <td>{{$value->nomor_telepon}}</td>
                    <td>{{$value->jenis_kelamin}}</td>
                    <td>
                        <a href="{{ route('pasien_checkup', $value->id) }}" style="float: left;" class="btn btn-success btn-sm mr-2" data-toggle="tooltip" data-placement="left" title="Check Up Pasien"><i class="fas fa-user-md"></i></a>
                        
                        <form action="{{ route('pasien_delete', $value->id) }}" method="post">
                            @csrf
                            @method("DELETE")
                            <button style="float: left;" type="submit" class="btn btn-danger btn-sm mr-2" data-toggle="tooltip" data-placement="left" title="Hapus Data Pasien" onclick="return confirm('Apakah anda yakin menghapus data ini.?')"><i class="fas fa-trash"></i></button>
                        </form>
                        <a href="{{route('pasien_show', $value->id)}}" style="float: left;" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="left" title="Detail Pasien"><i class="far fa-eye"></i></a>
                        
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{-- {{ $data->links() }} --}}
    </div>
</main>
@endsection

@section('js')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#pasien').DataTable();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        function asuransi(){
        document.getElementById('nama_asuransi').style.display ='block';
        document.getElementById('nomor_jaminan').style.display ='none';
        }
        function umum(){
        document.getElementById('nomor_jaminan').style.display ='none';
        document.getElementById('nama_asuransi').style.display ='none';
        }
        function bpjs(){
        document.getElementById('nomor_jaminan').style.display = 'block';
        document.getElementById('nama_asuransi').style.display ='none';
        }
    </script>
@endsection 