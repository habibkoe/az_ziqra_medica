@extends('layouts.login')

@section('content')
  <form class="form-signin" method="POST" action="{{ route('login') }}">
    @csrf
    
    <h1 class="h3 mb-3 font-weight-normal">Silaq Teme</h1>
    <label for="email" class="sr-only">{{ __('E-Mail Address') }}</label>
    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    <br>
    <label for="password" class="sr-only">{{ __('Password') }}</label>
    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    <div class="checkbox mb-3">
      <label>
        <input class="form-check-input" type="checkbox" name="remember-me" id="remember" {{ old('remember') ? 'checked' : '' }}> Remember me
      </label>
    </div>
    <button type="submit" class="btn btn-lg btn-primary btn-block">
            {{ __('Login') }}
    </button>
    <a class="btn btn-link" href="{{ route('password.request') }}">
        {{ __('Side Lupaq Password?') }}
    </a>
    <p class="mt-5 mb-3 text-muted">&copy; 2018</p>
  </form>
@endsection
