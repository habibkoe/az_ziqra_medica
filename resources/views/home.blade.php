@extends('layouts.app')

@section('content')
          <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 my-4">
            <h1 class="h2">Data Kunjungan Pasien</h1>
  
            <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
          </main>
@endsection

@section('js')
    <!-- Graphs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script>
      $.ajax({
               url: "http://127.0.0.1:8000/chart_kunjungan",
               method: "GET",
               success: function (data) {
                   console.log(data);
                    var pasien = [];
                    var bln = [];

                    for (var i in data) {
                      pasien.push(data[i].jml_pasien);
                      bln.push(data[i].bulan);
                    }

                    var chartdata = {
                         labels: ["Bulan " + bln],
                         datasets: [
                              {
                                   label: "jumlah pasien",
                                   fill: false,
                                   lineTension: 0.1,
                                   backgroundColor: "rgba(208, 197, 195, 0.75)",
                                   borderColor: "rgba(208, 197, 195, 1)",
                                   pointHoverBackgroundColor: "rgba(208, 197, 195, 1)",
                                   pointHoverBorderColor: "rgba(208, 197, 195, 1)",
                                   data: [pasien]
                              },
                         ]
                    };

                    var bar = $("#myChart");

                    var barGraph = new Chart(bar, {
                         type: 'bar',
                         data: chartdata,
                         options: {
                              scales: {
                                   yAxes: [{
                                             ticks: {
                                                  beginAtZero: true
                                             }
                                        }]
                              }
                         }
                    });

               },
               error: function (data) {
                    console.log(data);
               }
          });
    </script>
@endsection