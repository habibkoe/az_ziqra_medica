<nav class="col-md-2 d-none d-md-block bg-light sidebar">
        <div class="sidebar-sticky">
          <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link @if($sidebar=='home') active @endif" href="{{ route('home') }}">
                  <i class="fas fa-tachometer-alt"></i>
                Dashboard <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link @if($sidebar=='pasiens') active @endif" href="{{ route('pasiens') }}">
                <i class="fas fa-wheelchair"></i>
                Pasien
              </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if($sidebar=='check_up') active @endif" href="{{ route('pasien_checkup_all') }}">
                    <i class="fas fa-user-md"></i>
                  Pemeriksaan
                </a>
            </li>
            <li class="nav-item">
              <a class="nav-link @if($sidebar=='users') active @endif" href="{{ route('users') }}">
                <i class="fas fa-user-cog"></i>
                User
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link @if($sidebar=='report') active @endif" href="#">
                <i class="fas fa-chart-pie"></i>
                Reports
              </a>
            </li>
          </ul>
        </div>
      </nav>