@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <style>
        .telang {
            display: none;
        }
    </style>
@endsection

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 my-4">
    <div class="row">
        <div class="col-md-3">
                <a href="{{ route('pasien_checkup_new') }}" class="btn btn-success btn-sm"><i class="fas fa-user-md"></i> Check Up</a>
        </div>
        <div class="col-md-9">
                <a href="{{ route('export_pemeriksaan') }}" class="btn btn-primary btn-sm"><i class="fas fa-print"></i></a>
            {{-- <form action="{{ route('export_pemeriksaan') }}" method="POST">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group row">
                            <label for="tanggal_awal" class="col-md-3 col-form-label">Dari</label>
                            <div class="col-md-9">
                                <input type="date" name="dari_tanggal" id="dari_tanggal" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group row">
                            <label for="tanggal_ahir" class="col-md-3 col-form-label">Sampai</label>
                            <div class="col-md-9">
                                <input type="date" name="sampai_tanggal" id="sampai_tanggal" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success btn-sm"><i class="fas fa-print"></i></button>
                    </div>
                </div>
            </form> --}}
        </div>
    </div>
    <hr>
    <br>
    <div class="table-responsive">
        <table id="pasien" class="table table-bordered table-hover table-sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Jenis Pemeriksaan</th>
                    <th>Resep Obat</th>
                    <th>Tanggal Periksa</th>
                    <th>Setting</th>
                </tr>
            </thead>
            <tbody>
                @foreach($history_checkup as $index => $value)
                <tr>
                    <td>{{$index+1}}</td>
                    <td>{{$value->pasien->nama_awal}} {{$value->pasien->nama_ahir}}</td>
                    <td>{{$value->jenis_pemeriksaan}}</td>
                    <td>{{$value->resep_obat}}</td>
                    <td>{{ date("d-m-Y", strtotime($value->created_at)) }}</td>
                    <td>
                        <a href="{{route('pasien_show', $value->pasien->id)}}" style="float: left;" class="btn btn-primary btn-sm mr-2" data-toggle="tooltip" data-placement="left" title="Detail Pasien"><i class="far fa-eye"></i></a>
                        
                        <button style="float: left;" type="button" class="btn btn-warning btn-sm mr-2" data-toggle="modal"
                            data-target="#edit_pemeriksaan_{{$value->id}}" title="Edit"><i class="far fa-edit" data-toggle="tooltip" data-placement="left" title="Edit Pemeriksaan Pasien"></i></button>
                        {{-- modaal edit pasien --}}
                        <div class="modal fade" id="edit_pemeriksaan_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="edit_pemeriksaan_{{$value->id}}" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Pemeriksaan {{$value->pasien->nama_awal}} {{$value->pasien->nama_ahir}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                                <form method="POST" action="{{ route('pasien_checkup_store') }}">
                                                        @csrf
                                                        <div class="form-group row">
                                                            <label for="id_pasien" class="col-md-4 col-form-label text-md-right">Nama Pasien</label>
                                                
                                                            <div class="col-md-6">
                                                                <input type="hidden" name="id_pasien" value="{{$value->pasien->id}}">
                                                                <input id="nama_pasien" type="text" class="form-control{{ $errors->has('nama_pasien') ? ' is-invalid' : '' }}"
                                                                name="nama_pasien" value="{{$value->pasien->nama_awal}} {{$value->pasien->nama_ahir}}" disabled>
                                                            </div>
                                                        </div>
                                                
                                                        <div class="form-group row">
                                                                <label for="jenis_pemeriksaan" class="col-md-4 col-form-label text-md-right">Jenis Pemeriksaan</label>
                                                    
                                                                <div class="col-md-6">
                                                                    <select name="jenis_pemeriksaan" id="jenis_pemeriksaan" class="form-control" required autofocus>
                                                                        <option value="Fisik">Fisik</option>
                                                                        <option value="Terapi">Terapi</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                
                                                        <div class="form-group row">
                                                            <label for="diagnosa" class="col-md-4 col-form-label text-md-right">Diagnosa</label>
                                                
                                                            <div class="col-md-6">
                                                            <textarea name="diagnosa" id="diagnosa" cols="30" rows="3" class="form-control" required>{{$value->diagnosa}}</textarea>
                                                
                                                                @if ($errors->has('diagnosa'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('diagnosa') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                
                                                        <div class="form-group row">
                                                            <label for="tindakan_medis" class="col-md-4 col-form-label text-md-right">Tindakan Medis</label>
                                                
                                                            <div class="col-md-6">
                                                                <input id="tindakan_medis" type="text" class="form-control{{ $errors->has('tindakan_medis') ? ' is-invalid' : '' }}"
                                                                    name="tindakan_medis" required value="{{$value->tindakan_medis}}">
                                                
                                                                @if ($errors->has('tindakan_medis'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('tindakan_medis') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                
                                                        <div class="form-group row">
                                                            <label for="resep_obat" class="col-md-4 col-form-label text-md-right">Resep Obat</label>
                                                
                                                            <div class="col-md-6">
                                                                <input id="resep_obat" type="text" class="form-control{{ $errors->has('resep_obat') ? ' is-invalid' : '' }}"
                                                                    name="resep_obat" required value="{{$value->resep_obat}}">
                                                
                                                                @if ($errors->has('resep_obat'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('resep_obat') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                
                                                        <div class="form-group row">
                                                            <label for="alamat" class="col-md-4 col-form-label text-md-right">Apakah Pasien Dirujuk.?</label>
                                                
                                                            <div class="col-md-6">
                                                                <input type="radio" name="isDirujuk" id="isDirujuk" value="1" onclick="show2({{$value->id}});"> Ya
                                                                <br>
                                                                <input type="radio" name="isDirujuk" id="isDirujuk" value="0" onclick="show1({{$value->id}});"> Tidak
                                                            </div>
                                                        </div>
                                                        <div id="div1{{$value->id}}" class="telang">
                                                            <div class="form-group row">
                                                                <label for="lokasi_rujukan" class="col-md-4 col-form-label text-md-right">Lokasi Rujukan</label>
                                                
                                                                <div class="col-md-6">
                                                                    <input id="lokasi_rujukan" type="text" class="form-control{{ $errors->has('lokasi_rujukan') ? ' is-invalid' : '' }}"
                                                                        name="lokasi_rujukan" value="{{$value->lokasi_rujukan}}">
                                                
                                                                    @if ($errors->has('lokasi_rujukan'))
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('lokasi_rujukan') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                
                                                            <div class="form-group row">
                                                                <label for="alasan_dirujuk" class="col-md-4 col-form-label text-md-right">Alasan Dirujuk</label>
                                                
                                                                <div class="col-md-6">
                                                                <textarea name="alasan_dirujuk" id="alasan_dirujuk" cols="30" rows="5" class="form-control">{{$value->alasan_dirujuk}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                        
                                                <div class="form-group row mb-0">
                                                    <div class="col-md-6 offset-md-4">
                                                        <button type="submit" class="btn btn-primary">
                                                            {{ __('Simpan') }}
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {{-- end modal edit pasien --}}
                        
                        <form action="{{ route('pasien_delete', $value->id) }}" method="post">
                            @csrf
                            @method("DELETE")
                            <button style="float: left;" type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="left" title="Hapus Data Pasien" onclick="return confirm('Apakah anda yakin menghapus data ini.?')"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{-- {{ $data->links() }} --}}
    </div>
</main>
@endsection

@section('js')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#pasien').DataTable();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        function show1(id){
            document.getElementById('div1'+ id).style.display ='none';
        }
        function show2(id){
            document.getElementById('div1' + id).style.display = 'block';
        }
    </script>
@endsection 