@extends('layouts.app')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <style>
        .telang {
            display: none;
        }
    </style>
@endsection

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 my-4">
    <h2>Check Up Pasien</h2>
    <form method="POST" action="{{ route('pasien_checkup_store') }}">
        @csrf
        <div class="form-group row">
            <label for="id_pasien" class="col-md-4 col-form-label text-md-right">Pasien</label>

            <div class="col-md-6">
                    <select name="id_pasien" id="id_pasien" class="form-control" required autofocus>
                        @foreach ($pasiens as $item)
                            <option value="{{$item->id}}">{{$item->kode_pasien}}::{{$item->nama_awal}} {{$item->nama_ahir}}</option>
                        @endforeach
                    </select>
            </div>
        </div>

        <div class="form-group row">
                <label for="jenis_pemeriksaan" class="col-md-4 col-form-label text-md-right">Jenis Pemeriksaan</label>
    
                <div class="col-md-6">
                    <select name="jenis_pemeriksaan" id="jenis_pemeriksaan" class="form-control" required autofocus>
                        <option value="Fisik">Fisik</option>
                        <option value="Terapi">Terapi</option>
                    </select>
                </div>
            </div>

        <div class="form-group row">
            <label for="diagnosa" class="col-md-4 col-form-label text-md-right">Diagnosa</label>

            <div class="col-md-6">
                <textarea name="diagnosa" id="diagnosa" cols="30" rows="3" class="form-control" required></textarea>

                @if ($errors->has('diagnosa'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('diagnosa') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="tindakan_medis" class="col-md-4 col-form-label text-md-right">Tindakan Medis</label>

            <div class="col-md-6">
                <input id="tindakan_medis" type="text" class="form-control{{ $errors->has('tindakan_medis') ? ' is-invalid' : '' }}"
                    name="tindakan_medis" required>

                @if ($errors->has('tindakan_medis'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('tindakan_medis') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="resep_obat" class="col-md-4 col-form-label text-md-right">Resep Obat</label>

            <div class="col-md-6">
                <input id="resep_obat" type="text" class="form-control{{ $errors->has('resep_obat') ? ' is-invalid' : '' }}"
                    name="resep_obat" required>

                @if ($errors->has('resep_obat'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('resep_obat') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="alamat" class="col-md-4 col-form-label text-md-right">Apakah Pasien Dirujuk.?</label>

            <div class="col-md-6">
                <input type="radio" name="isDirujuk" id="isDirujuk" value="1" onclick="show2();"> Ya
                <br>
                <input type="radio" name="isDirujuk" id="isDirujuk" value="0" onclick="show1();"> Tidak
            </div>
        </div>
        <div id="div2" class="telang">
            <div class="form-group row">
                <label for="lokasi_rujukan" class="col-md-4 col-form-label text-md-right">Lokasi Rujukan</label>

                <div class="col-md-6">
                    <input id="lokasi_rujukan" type="text" class="form-control{{ $errors->has('lokasi_rujukan') ? ' is-invalid' : '' }}"
                        name="lokasi_rujukan">

                    @if ($errors->has('lokasi_rujukan'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('lokasi_rujukan') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="alasan_dirujuk" class="col-md-4 col-form-label text-md-right">Alasan Dirujuk</label>

                <div class="col-md-6">
                    <textarea name="alasan_dirujuk" id="alasan_dirujuk" cols="30" rows="5" class="form-control"></textarea>
                </div>
            </div>
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                    <a href="{{url('check_up')}}" class="btn btn-secondary">Batal</a>
                <button type="submit" class="btn btn-primary">
                    {{ __('Simpan') }}
                </button>
            </div>
        </div>
    </form>
</main>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#id_pasien').select2();
        });
        function show1(){
        document.getElementById('div2').style.display ='none';
        }
        function show2(){
        document.getElementById('div2').style.display = 'block';
        }
    </script>
@endsection