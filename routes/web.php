<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/pasien/{id}', 'PasienController@view');
    
    Route::get('/users', 'UserController@index')->name('users');
    Route::post('/users/new', 'UserController@store')->name('user_new');
    Route::delete('/users/{id}', 'UserController@destroy')->name('user_delete');
    Route::put('/users/{id}', 'UserController@update')->name('user_edit');

    Route::get('/pasiens', 'PasienController@index')->name('pasiens');
    Route::get('/pasiens/{id}', 'PasienController@show')->name('pasien_show');    
    Route::post('/pasiens/new', 'PasienController@store')->name('pasien_new');
    Route::put('/pasiens/{id}', 'PasienController@update')->name('pasien_edit');
    Route::delete('/pasiens/{id}', 'PasienController@destroy')->name('pasien_delete');

    Route::get('/check_up', 'PemeriksaanController@index')->name('pasien_checkup_all');
    Route::get('/check_up/{id}', 'PemeriksaanController@edit')->name('pasien_checkup');
    Route::get('/check_up_new', 'PemeriksaanController@create')->name('pasien_checkup_new');    
    Route::post('/check_up/store', 'PemeriksaanController@store')->name('pasien_checkup_store');
    Route::get('/export_pemeriksaan', 'PemeriksaanController@export')->name('export_pemeriksaan');

    //JSON CHART
    Route::get('/chart_kunjungan', 'PemeriksaanController@chartKunjungan')->name('chart_kunjungan');
});


// Route::get('/pasien/{id}', 'PasienController@view')->middleware('admin');
// Route::get('/users', 'UserController@index')->name('users')->middleware('admin');
// Route::post('/users/new', 'UserController@store')->name('user_new')->middleware('admin');
// Route::delete('/users/{id}', 'UserController@destroy')->name('user_delete')->middleware('admin');
// Route::put('/users/{id}', 'UserController@update')->name('user_edit')->middleware('admin');
// Route::get('/pasiens', 'PasienController@index')->name('pasiens')->middleware('admin');



// Route::middleware(['perawat', 'admin'])->group(function () {
//     Route::get('/pasiens', 'PasienController@index')->name('pasiens');
// });